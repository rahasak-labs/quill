name := "quill"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  Seq(
    "com.typesafe.akka"         %% "akka-actor"             % "2.4.14",
    "com.typesafe.akka"         %% "akka-slf4j"             % "2.4.14",
    "com.datastax.cassandra"    % "cassandra-driver-core"   % "3.1.1",
    "io.getquill"               %% "quill-cassandra"        % "3.4.10",
    "org.slf4j"                 % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"            % "logback-classic"         % "1.0.9",
    "org.scalatest"             % "scalatest_2.11"          % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
