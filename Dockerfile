#FROM ubuntu:18.04
FROM resin/rpi-raspbian:jessie

MAINTAINER Eranga Bandara(erangaeb@gmail.com)

# install required packages
RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN apt-get install -y wget

# install java
RUN apt-get install -y openjdk-8-jdk
RUN rm -rf /var/lib/apt/lists/*

# set JAVA_HOME
#ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-armhf

# working directory
WORKDIR /app

# copy file
ADD target/scala-2.11/quill-assembly-1.0.jar quill.jar

# command
ENTRYPOINT [ "java", "-jar", "/app/quill.jar" ]
